﻿using Types;
using UI;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    
    [SerializeField] private GameObject ballPrefab;

    [SerializeField] private Paddle playerPaddle;
    
    [SerializeField] private ComputerPaddle computerPaddle;


    private GameObject ballInstance;
    public Ball BallInstance
    {
        get
        {
            if (ballInstance == null)
            {
                ballInstance = Instantiate(ballPrefab);
            }

            return ballInstance.GetComponent<Ball>();
        }
    }
    
    public int PlayerScore { get; private set; }
    public int ComputerScore { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
    
    private void Start()
    {
        UIManager.Instance.SetGuiState(GuiState.Main);
        // NewGame();
    }

    /*
    private static void OnInputUpdate(Vector2 obj)
    {
        throw new System.NotImplementedException();
    }
    */
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R)) {
            NewGame();
        }
    }

    public void NewGame()
    {
        computerPaddle.ball = BallInstance.Rigidbody;
        
        SetPlayerScore(0);
        SetComputerScore(0);
        StartRound();
    }

    public void StartRound()
    {
        UIManager.Instance.SetGuiState(GuiState.GameScreen);
        
        playerPaddle.ResetPosition();
        computerPaddle.ResetPosition();
        BallInstance.ResetPosition();
        BallInstance.AddStartingForce();
    }

    public void PlayerScores()
    {
        SetPlayerScore(PlayerScore + 1);
        StartRound();
    }

    public void ComputerScores()
    {
        SetComputerScore(ComputerScore + 1);
        StartRound();
    }

    private void SetPlayerScore(int score)
    {
        PlayerScore = score;
        UIManager.Instance.GamePlayScreen.GetComponent<GamePlayScreenController>().SetScore(true, score);
    }

    private void SetComputerScore(int score)
    {
        ComputerScore = score;
        UIManager.Instance.GamePlayScreen.GetComponent<GamePlayScreenController>().SetScore(false, score);
    }
}

using System;
using System.Collections.Generic;
using Types;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    [SerializeField] private Canvas root;
    [SerializeField] private GameObject mainScreen;
    [SerializeField] private GameObject gamePlayScreen;

    private Dictionary<string, GameObject> cache = new Dictionary<string, GameObject>();

    public GuiState CurrentGuiStatus
    {
        get;
        private set;
    } = GuiState.None;
    
    internal GameObject MainScreen
    {
        get { return GetInstance(nameof(MainScreen), mainScreen); }
    }

    internal GameObject GamePlayScreen
    {
        get { return GetInstance(nameof(GamePlayScreen), gamePlayScreen); }
    }

    private GameObject GetInstance(string key, GameObject prefabRef)
    {
        GameObject screenInstance = null;

        if (!cache.TryGetValue(key, out screenInstance) || screenInstance == null)
        {
            screenInstance = Instantiate(prefabRef, root.transform);

            if (!cache.ContainsKey(key))
            {
                cache.Add(key, screenInstance);
            }
            else
            {
                cache[key] = screenInstance;
            }
        }

        return screenInstance;
    }

    // private bool TryGetSomething(out GameObject sth)
    // {
    //     // sth = GameObject.Find("dfsfsdf");
    //
    //     return false;
    // }
    //
    // private bool TryGetSomething(ref GameObject sth)
    // {
    //     // sth = GameObject.Find("dfsfsdf");
    //
    //     return false;
    // }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    public void SetGuiState(GuiState newState)
    {
        CurrentGuiStatus = newState;
        
        switch (newState)
        {
            case GuiState.GameScreen:
                GamePlayScreen.SetActive(true);
                
                if (mainScreen)
                {
                    // MainScreen.SetActive(false);
                    Destroy(MainScreen);
                }
                break;
            case GuiState.Main:
                MainScreen.SetActive(true);
                
                if (gamePlayScreen)
                {
                    // GamePlayScreen.SetActive(false);
                    Destroy(GamePlayScreen);
                }
                break;

            default:
                throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
        }
    }
}
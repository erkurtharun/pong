using System;
using UnityEngine;


namespace Managers
{
    public class InputManager : MonoBehaviour
    {
        public event Action<Vector2> OnInputUpdate;
        internal Vector2 direction;
    
        public static InputManager Instance;

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(this);
            }
            else
            {
                Instance = this;
            }
        }

        private void Update()
        {
            direction.y = Input.GetAxis("Vertical");
            direction.x = Input.GetAxis("Horizontal");
            OnInputUpdate?.Invoke(direction);
        }
    }
}


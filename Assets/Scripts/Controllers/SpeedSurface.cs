using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class SpeedSurface : MonoBehaviour
{
    public float bounceStrength = 1f;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var ball = collision.gameObject.GetComponent<Ball>();

        if (ball == null) return;
        // Apply a force to the ball in the opposite direction of the
        // surface to make it bounce off
        var normal = collision.GetContact(0).normal;
        ball.Rigidbody.AddForce(-normal * bounceStrength, ForceMode2D.Impulse);
    }

}

﻿using System;
using Managers;
using UnityEngine;

public class PlayerPaddle : Paddle
{
    public Vector2 Direction { get; private set; }

    private void Start()
    {
        InputManager.Instance.OnInputUpdate += OnInputUpdate;
    }

    private void OnInputUpdate(Vector2 direction)
    {
        Direction = Vector2.up * direction.y;
        //
        // if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) {
        //     Direction = Vector2.up;
        // } else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) {
        //     Direction = Vector2.down;
        // } else {
        //     Direction = Vector2.zero;
        // }
    }

    // private void Update()
    // {
    //     if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) {
    //         Direction = Vector2.up;
    //     } else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) {
    //         Direction = Vector2.down;
    //     } else {
    //         Direction = Vector2.zero;
    //     }
    // }

    private void FixedUpdate()
    {
        if (Direction.sqrMagnitude != 0)
        {
            rBodyComponent.AddForce(Direction * speed);
        }
    }

}

﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(Rigidbody2D))]
public class Paddle : MonoBehaviour
{
    [SerializeField] protected Rigidbody2D rBodyComponent;
    
    public float speed = 20f;

    private void OnValidate()
    {
        rBodyComponent = GetComponent<Rigidbody2D>();
    }

    public void ResetPosition()
    {
        rBodyComponent.velocity = Vector2.zero;
        rBodyComponent.position = new Vector2(rBodyComponent.position.x, 0f);
    }

}

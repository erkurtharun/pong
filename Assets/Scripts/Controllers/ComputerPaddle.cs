using System;
using Types;
using UnityEngine;

public class ComputerPaddle : Paddle
{
    internal Rigidbody2D ball;

    private Rigidbody2D Ball
    {
        get
        {
            if (ball == null)
            {
                var b = FindObjectOfType<Ball>();
    
                if (b != null)
                {
                    ball = b.GetComponent<Rigidbody2D>();    
                }
            }
    
            return ball;
        }
        set
        {
            ball = value;
        }
    }
    
    private void FixedUpdate()
    {
        if (UIManager.Instance.CurrentGuiStatus != GuiState.GameScreen)
        {
            return;
        }
        
        if (this.Ball.velocity.x > 0.0f)
        {
            if (this.Ball.position.y > this.transform.position.y)
            {
                rBodyComponent.AddForce(Vector2.up * this.speed);
            }else if (this.Ball.position.y < this.transform.position.y)
            {
                rBodyComponent.AddForce(Vector2.down * this.speed);
            }
        }
        else
        {
            if (this.transform.position.y > 0.0f)
            {
                rBodyComponent.AddForce(Vector2.down * this.speed);
            } else if (this.transform.position.y < 0.0f)
            {
                rBodyComponent.AddForce(Vector2.up * this.speed);
            }
        }
    }
}

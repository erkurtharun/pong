﻿using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Rigidbody2D))]
public class Ball : MonoBehaviour
{
    public float speed = 100f;
    // public float bounceStrength = 0.001f;
    public Rigidbody2D Rigidbody { get; private set; }

    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        ResetPosition();
    }

    public void ResetPosition()
    {
        Rigidbody.velocity = Vector2.zero;
        Rigidbody.position = Vector2.zero;
    }

    public void AddStartingForce()
    {
        
        var x = Random.value < 0.5f ? -1f : 1f;
        var y = Random.value < 0.5f ? Random.Range(-1f, -0.5f)
                                      : Random.Range(0.5f, 1f);

        var direction = new Vector2(x, y);
        Rigidbody.AddForce(direction * speed);
    }

    public void AddForce(Vector2 force)
    {
        Rigidbody.AddForce(force);
        
    }

    // private void FixedUpdate()
    // {
    //     Debug.Log(Rigidbody.velocity);
    // }
    
    /*
    private void OnCollisionEnter2D(Collision2D collision)
    {
        var normal = collision.GetContact(0).normal;
        Rigidbody.AddForce(normal * bounceStrength, ForceMode2D.Impulse);
    }
    */
}

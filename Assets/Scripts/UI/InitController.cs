using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitController : MonoBehaviour
{
    // Wait for 5 seconds and display the bar loading.
    // After 5 seconds load "Pong" scene

    private void Start()
    {
        SceneManager.LoadScene("Pong");
    }

    // Update is called once per frame
    void Update()
    {
        //
    }
}

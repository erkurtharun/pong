﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class GamePlayScreenController : MonoBehaviour
    {
        [SerializeField] private Text playerScoreText;
        [SerializeField] private Text computerScoreText;

        public void SetScore(bool isPlayer, int newScore)
        {
            if (isPlayer)
            {
                playerScoreText.text = newScore.ToString();
            }
            else
            {
                computerScoreText.text = newScore.ToString();
            }
        }
    }
}
﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class MainScreenController : MonoBehaviour
    {
        [SerializeField] private Button playButton;

        private void Awake()
        {
            playButton.onClick.AddListener(StartGamePlay);
        }

        private void StartGamePlay()
        {
            GameManager.Instance.StartRound();
        }
    }
}